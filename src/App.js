import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import { AddTodoAction, RemoveTodoAction } from "./actions/TodoActions";

function App() {
  const [todo, setTodo] = useState();
  const dispatch = useDispatch();
  const Todo = useSelector((state) => state.Todo);
  const { todos } = Todo;
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(AddTodoAction(todo));
  };

  const removeHandler = (t) => {
    dispatch(RemoveTodoAction(t));
  };

  return (
    <div className="App">
      <header className="App-header">
        <h2>Todo List App in Redux</h2>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Enter a Todo"
            style={{
              width: 350,
              padding: 10,
              borderRadius: 20,
              border: "none",
              fontSize: 20,
            }}
            onChange={(e) => setTodo(e.target.value)}
          />

          <button
            type="submit"
            style={{
              padding: 12,
              borderRadius: 25,
              marginLeft: 20,
              fontSize: 15,
            }}
          >
            Go
          </button>
        </form>
        <ul className="all-todos">
          {todos &&
            todos.map((t) => {
              return (
                <li className="single-todo" key={t.id}>
                  <span className="todo-text">{t.todo}</span>
                  <button
                    style={{
                      padding: 10,
                      borderRadius: 25,
                      border: "1px solid white",
                      color: "white",
                      backgroundColor: "orangered",
                    }}
                    onClick={() => removeHandler(t)}
                  >
                    Delete
                  </button>
                </li>
              );
            })}
        </ul>
      </header>
    </div>
  );
}

export default App;
