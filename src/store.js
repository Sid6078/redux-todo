import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { thunk } from "redux-thunk";
import TodoReducers from "./reducers/TodoReducers";

const reducer = combineReducers({
  // Contains all reducers
  Todo: TodoReducers,
});

const initialState = {};

const middleware = [thunk];

export const store = configureStore({
  reducer,
  preloadedState: initialState,
  middleware: () => [...middleware],
  devTools: true,
});
